<?php

// TRI-STATE-FROM

function getForm($name = "", $age = "") {

    $form = <<< ABC123DOREMI
<form>
    Name: <input type="text" name="name" value="$name"><br>
    Age: <input type="number" name="age" value="$age"><br>
    <input type="submit" value="Introduce yourself">
</form>
ABC123DOREMI;
    return $form;
}

if (isset($_GET['name'])) {
    // receiving a submision
    // extract values submitted
    $name = $_GET['name'];
    $age = $_GET['age'];

    // flag
    $errorList = array();
    // verify
    if (strlen($name) < 2 || strlen($name) > 20) {
        array_push($errorList, "Error: name must be between 2 and 20 characters long");
        $name = "";
    }
    //
    if (empty($age) || $age < 0 || $age > 150) {
        array_push($errorList, "Error: age must be between 0 and 150");
        $age = "";
    }

    //
    if (!$errorList) {
        // STATE 2: SUCCESSFUL SUBMISSION
        echo "<p>Hi $name, you are $age y/o</p>";
    } else {
        // STATE 3: FAILED SUBMISSION
        echo "<ul>\n";
        foreach ($errorList as $error) {
            echo '<li class="errorItem">' . $error . "</li>\n";
        }
        echo "</ul>\n";
        echo getForm($name, $age);
    }
} else {
    // STATE 1: FIRST SHOW
    echo getForm();
}
