    <?php

    $link = mysqli_connect('localhost', 'blog1', 'dnQZnaDykiIv6hOH', 'blog1');
    if (!$link) {
        echo "Error: Unable to connect to MySQL." . PHP_EOL;
        echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
        echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
        exit;
    }

// TRI-STATE-FROM
    function getForm($username = "", $email = "") {

        $form = <<< ABC123DOREMI
<form method="post">
    Username: <input type="text" name="username" value="$username"><br>
    Email: <input type="email" name="email" value="$email"><br>
    Password: <input type="password" name="pass1"><br>
    Password (repeated): <input type="password" name="pass2"><br>
    <input type="submit" value="Register">
</form>
ABC123DOREMI;
        return $form;
    }

    if (isset($_POST['username'])) {
        // receiving a submision
        // extract values submitted
        $username = $_POST['username'];
        $email = $_POST['email'];
        $pass1 = $_POST['pass1'];
        $pass2 = $_POST['pass2'];

        // flag
        $errorList = array();
        // verify
        if (strlen($username) < 2 || strlen($username) > 10) {
            array_push($errorList, "Error: username must be between 2 and 10 characters long");
            $username = "";
        }
        if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
            array_push($errorList, "Error: email is invalid");
            $email = "";
        }
        //
        if ($pass1 != $pass2 || strlen($pass1) < 8) {
            array_push($errorList, "Error: passwords must match and be at least 8 characters long");
        }
        //
        if (!$errorList) {
            // STATE 2: SUCCESSFUL SUBMISSION
            $query = sprintf("INSERT INTO users VALUES (NULL, '%s', '%s', '%s')",
                    mysqli_real_escape_string($link, $username),
                    mysqli_real_escape_string($link, $email),
                    mysqli_real_escape_string($link, $pass1));
            $result = mysqli_query($link, $query);
            if (!$result) {
                die("<b>SQL query error: " . mysqli_error($link) . "</b>");
            }
            echo "<p>Registration successful</p>";
        } else {
            // STATE 3: FAILED SUBMISSION
            echo "<ul>\n";
            foreach ($errorList as $error) {
                echo '<li class="errorItem">' . $error . "</li>\n";
            }
            echo "</ul>\n";
            echo getForm($username, $email);
        }
    } else {
        // STATE 1: FIRST SHOW
        echo getForm();
    }
