<?php
session_start();

if (!isset($_SESSION['user'])) {
    $_SESSION['user'] = array();
}

$link = mysqli_connect('localhost', 'blog1', 'dnQZnaDykiIv6hOH', 'blog1');
if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

// TRI-STATE-FROM
function getForm($username = "", $email = "") {

    $form = <<< ABC123DOREMI
<form method="post">
    Username: <input type="text" name="username"><br>
    Password: <input type="password" name="password"><br>
    <input type="submit" value="Login">
</form>

ABC123DOREMI;
    return $form;
}

if (isset($_POST['username'])) {
    // receiving a submision
    // extract values submitted
    $username = $_POST['username'];
    $password = $_POST['password'];

    // flag
    $hasError = false;

    $query = sprintf("SELECT * FROM users WHERE username='%s'",
            mysqli_real_escape_string($link, $username));
    $result = mysqli_query($link, $query);
    if (!$result) {
        die("<b>SQL query error: " . mysqli_error($link) . "</b>");
    }
    $row = mysqli_fetch_assoc($result);
    if (is_null($row)) {
        $hasError = true;
    } else { // user exists
        if ($row['password'] != $password) {
            $hasError = true;
        }
    }
    //
    if ($hasError) {
        echo "<p>Error: invalid username or password.</p>\n";
        echo getForm();
    } else {
        unset($row['password']);
        $_SESSION['user'] = $row;
        echo "<p>You're logged in</p>";
    }


} else {
    // STATE 1: FIRST SHOW
    echo getForm();
}
